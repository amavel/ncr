package utils;

import objects.OrderedIntSet;

public class CompareSets {

    public static int IS_SMALLER_SET = 0;
    public static int IS_EQUAL_SET   = 1;
    public static int IS_BIGGER_SET  = 2;

    /**
     * Compares two sets of ordered integers
     * @param set1 First set
     * @param set2 Second set
     * @return How the two sets compare
     */
    public static int compareOrderedSets(OrderedIntSet set1, OrderedIntSet set2) {
        if (set1.size() < set2.size())
            return IS_SMALLER_SET;
        else if (set1.size() > set2.size())
            return IS_BIGGER_SET;
        else {
            for(int i=0; i<set1.size(); i++) {
                if (set1.get(i) < set2.get(i))
                    return IS_SMALLER_SET;
                else if (set1.get(i) > set2.get(i))
                    return IS_BIGGER_SET;
            }
        }
        return IS_EQUAL_SET;
    }

}
