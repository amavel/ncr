package objects;

import utils.CompareSets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Set of integers kept sorted in ascending order
 */
public class OrderedIntSet {

    private ArrayList<Integer> mElements;

    /**
     * Empty constructor
     */
    public OrderedIntSet() {
        mElements = new ArrayList<>();
    }

    /**
     * Constructor from a string
     * @param string Comma-separated integers in a string
     */
    public OrderedIntSet(String string) {
        this();

        List<String> stringItems = Arrays.asList(string.split("\\s*,\\s*"));

        for (String intString : stringItems) {
            add(Integer.parseInt(intString));
        }
    }

    /**
     * Adds an integer to the set, keeping order
     * @param element A single element
     */
    public void add(int element) {

        for (int i=0; i<mElements.size(); i++) {
            if (element < mElements.get(i)) {
                mElements.add(i, element);
                return;
            }
        }
        mElements.add(element);
    }

    /**
     * Hash code of the class
     * @return Hash code
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        for (Integer number : mElements) {
            result = prime * result + number;
        }
        return result;
    }

    /**
     * Function to compare OrderedIntSets
     * @param obj object to be compared to
     * @return true if sets are duplicates, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return CompareSets.compareOrderedSets(this, (OrderedIntSet) obj) == CompareSets.IS_EQUAL_SET;
    }

    /**
     * Writes set in a readable form
     * @return String describing the set
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (Integer number : mElements) {
            sb.append(number != null ? number.toString() : "");
            sb.append(" ");
        }
        return sb.toString();
    }

    /**
     * Obtains an element at a certain index
     * @param index Index of the desired element
     * @return Element of the list at the desired index
     */
    public int get(int index) {
        assert (index < mElements.size());

        return mElements.get(index);
    }

    /**
     * Size of the set
     * @return Set size
     */
    public int size() {
        return mElements.size();
    }

}
