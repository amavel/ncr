package objects;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Class handling list of ordered sets of integers
 */
public class SetList {

    private HashMap<OrderedIntSet, Integer> mSetList;
    private ArrayList<String> mInvalidStrings;

    private int mDuplicates;
    private int mNonDuplicates;

    /**
     * Empty constructor
     */
    public SetList() {
        mSetList = new HashMap<>();
        mInvalidStrings = new ArrayList<>();

        mDuplicates    = 0;
        mNonDuplicates = 0;
    }

    /**
     * Constructor from a text file
     * @param fileReader File to be read. The file must be composed by comma-separated integer sets, each placed on a single line
     */
    public SetList(FileReader fileReader){
        this();

        BufferedReader br = new BufferedReader(fileReader);
        String strLine;

        try {
            while ((strLine = br.readLine()) != null) {
                try {
                    add(new OrderedIntSet(strLine));
                }
                catch (NumberFormatException exception) {
                    mInvalidStrings.add(strLine);
                }
            }

            br.close();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Adds an OrderedIntSet to the list
     * @param set OrderedIntSet to be added to the list
     * @return true if is a duplicate, false otherwise
     */
    public boolean add(OrderedIntSet set) {

        int count = mSetList.containsKey(set) ? mSetList.get(set) + 1 : 1;
        if (count>1) {
            mDuplicates++;
            mSetList.put(set, count);
            return true;
        }
        mNonDuplicates++;
        mSetList.put(set, count);
        return false;
    }

    /**
     * Returns the number of duplicates in the list
     * @return Number of duplicates in the list
     */
    public int getNumberOfDuplicates() {
        return mDuplicates;
    }

    /**
     * Returns the number of non-duplicates in the list
     * @return Number of non-duplicates in the list
     */
    public int getNumberOfNonDuplicates() {
        return mNonDuplicates;
    }

    /**
     * Returns the list of invalid strings attempted to be added in the list
     * @return List of invalid strings
     */
    public ArrayList<String> getInvalidStrings() {
        return mInvalidStrings;
    }

    /**
     * Prints the list of invalid strings
     */
    public void printInvalidStrings() {
        for (String invalidString : mInvalidStrings) {
            System.out.println(invalidString);
        }
    }

    /**
     * Prints the list of ordered sets added to the list
     */
    public void printSetList() {
        for (Map.Entry<OrderedIntSet, Integer> entry : mSetList.entrySet()) {
            System.out.println("Freq: " + entry.getValue() + " -> " + entry.getKey().toString());
        }
    }

    /**
     * The OrderedIntSet which was added the most as a duplicate to the list
     * @return Most frequently added set of integers
     */
    public OrderedIntSet getMostFrequentSet() {
        int maxFrequency = -1;
        OrderedIntSet mostFrequent = new OrderedIntSet();

        for (Map.Entry<OrderedIntSet, Integer> entry : mSetList.entrySet()) {
            if(entry.getValue() > maxFrequency) {
                mostFrequent = entry.getKey();
                maxFrequency = entry.getValue();
            }
        }
        return mostFrequent;
    }
}
