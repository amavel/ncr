package tests;

import objects.OrderedIntSet;
import objects.SetList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import utils.CompareSets;

import java.io.*;

import static org.junit.Assert.*;

public class SetListTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    /**
     * Tests library with an input
     */
    @Test
    public void testWithInputFile(){
        FileReader fileReader;
        try {
            fileReader = new FileReader("input.txt");
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return;
        }

        SetList setList = new SetList(fileReader);

        assertEquals(27, setList.getNumberOfNonDuplicates());
        assertEquals(476, setList.getNumberOfDuplicates());
        assertEquals(4, setList.getInvalidStrings().size());
    }

    /**
     * Tests whether two sets are recognized as duplicates
     */
    @Test
    public void testDuplicates(){
        OrderedIntSet set1 = new OrderedIntSet("1,2,3");
        OrderedIntSet set2 = new OrderedIntSet("2,1,3");

        SetList setList = new SetList();
        setList.add(set1);
        setList.add(set2);

        assertEquals(1, setList.getNumberOfDuplicates());
        assertEquals(1, setList.getNumberOfNonDuplicates());
        assertEquals(0, setList.getInvalidStrings().size());
    }

    /**
     * Tests whether two sets are not recognized as duplicates
     */
    @Test
    public void testNonDuplicates(){
        OrderedIntSet set1 = new OrderedIntSet("1,2,3");
        OrderedIntSet set2 = new OrderedIntSet("1,2,3,1");

        SetList setList = new SetList();
        setList.add(set1);
        setList.add(set2);

        assertEquals(0, setList.getNumberOfDuplicates());
        assertEquals(2, setList.getNumberOfNonDuplicates());
        assertEquals(0, setList.getInvalidStrings().size());
    }

    /**
     * Tests whether the list identifies entry as invalid
     */
    @Test
    public void testInvalidEntry(){
        try {
            String fileName = "temp_inputs.txt";
            folder.newFile(fileName);
            FileWriter fw = new FileWriter(fileName);

            fw.write("A,B,C");
            fw.close();


            SetList setList = new SetList(new FileReader(fileName));

            assertEquals(0, setList.getNumberOfDuplicates());
            assertEquals(0, setList.getNumberOfNonDuplicates());
            assertEquals(1, setList.getInvalidStrings().size());
        }
        catch(IOException exception){
            exception.printStackTrace();
        }
    }

    /**
     * Tests if frequency of a certain entry is identified correctly
     */
    @Test
    public void testMostFrequentSet(){
        FileReader fileReader;
        try {
            fileReader = new FileReader("/Users/amavel/Downloads/ncr/input.txt");
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return;
        }

        SetList setList = new SetList(fileReader);

        assertEquals(CompareSets.IS_EQUAL_SET, CompareSets.compareOrderedSets(new OrderedIntSet("3,5,11,23,24,88,189"),
                setList.getMostFrequentSet()));
    }
}